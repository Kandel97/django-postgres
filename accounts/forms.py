# accounts/forms.py
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

#formulario para crear usuarios
class CustomUserCreationForm(UserCreationForm): 
    class Meta:
        model = get_user_model()
        fields = (
            "email",
            "username",
        )

#Formulario para modificar usuarios
class CustomUserChangeForm(UserChangeForm): 
     class Meta:
        model = get_user_model()
        fields = (
            "email",
            "username",
        )