# accounts/admin.py
from django.contrib import admin
from django.contrib.auth import get_user_model 
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm 

CustomUser = get_user_model()

class CustomUserAdmin(UserAdmin): 
    add_form = CustomUserCreationForm 
    #especificar con que formulario se trabaja
    form = CustomUserChangeForm 
    model = CustomUser
    list_display = [
        "email",
        "username",
        "is_superuser",
    ]
    
admin.site.register(CustomUser, CustomUserAdmin)

